<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        $a=10;
        $b=3;
        
        //va a mostrar punto 1 porque $a es par
        if(($a%2)==0){
            echo "punto 1";
        }else{
            echo "punto 2";
        }
        
        //muestra este porque esta fuera del if y tiene un echo
        echo "punto 3";
        
        //va a mostrar punto 2 porque $a es impar
        if(($b%2)==0){
            echo "punto 1";
        }else{
            echo "punto 2";
        }
        
        //muestra este porque esta fuera del if y tiene un echo
        echo "punto 3";
        ?>
    </body>
</html>
