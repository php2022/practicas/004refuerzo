<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        //inicializando array
        $a=[
            "Lunes"=>100,
            "Martes"=>150,
            "Miercoles"=>300,
            "Jueves"=>10,
            "Viernes"=>50
        ];
        
        $b="Lunes";
        //me muestra 100 porque $b es igual a Lunes, y Lunes en el array $a vale 100
        switch($b){
            case "Lunes":
                echo $a["Lunes"];
                break;
            case "Martes":
                echo $a["Martes"];
                break;
            case "Miercoles":
                echo $a["Miercoles"];
                break;
            case "Jueves":
                echo $a["Jueves"];
                break;
            case "Viernes":
                echo $a["Viernes"];
                break;
            default:
                echo "no se";
                break;
        }
        ?>
    </body>
</html>
