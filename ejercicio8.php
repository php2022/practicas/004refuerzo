<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        $dni=71279128;
        $letraUsuario="J";
        $letras=['T', 'R', 'W', 'A', 'G', 'M', 'Y', 'F', 'P', 'D', 'X', 'B', 'N', 'J', 'Z', 'S', 'Q', 'V', 'H', 'L', 'C', 'K', 'E', 'T'];
        
        if($dni<99999999){
            $resto=$dni%23;
            $letra=$letras[$resto];
            echo $letra;
        }else{
            echo "El DNI introducido no existe";
        }
        
        if($letraUsuario!=$letra){
            echo "<br>La letra introducida NO coincide con la letra del DNI";
        }else{
            echo "La letra introducida coincide con la letra del DNI";
        }
        ?>
    </body>
</html>
